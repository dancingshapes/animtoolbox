# ***** BEGIN GPL LICENSE BLOCK *****
#
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ***** END GPL LICENCE BLOCK *****

bl_info = {
    "name": "AnimToolBox",
    "author": "Tal Hershkovich",
    "version" : (0, 0, 0, 4),
    "blender" : (3, 2, 0),
    "location": "View3D - Properties - Animation Panel",
    "description": "A set of animation tools",
    "warning": "Still Alpha",
    "wiki_url": "",
    "category": "Animation"}

if "bpy" in locals():
    import importlib
    if "Rigger_Toolbox" in locals():
        importlib.reload(Rigger_Toolbox)
    if "BakeToCtrl" in locals():
        importlib.reload(BakeToCtrl)
    if "BakeToCtrl" in locals():
        importlib.reload(Tools)
#    if "addon_updater_ops" in locals():
#        importlib.reload(addon_updater_ops)

import bpy
#from . import addon_updater_ops
from . import BakeToCtrl
from . import Rigger_Toolbox
from . import Tools

    
class BakeToCtrlsSettings(bpy.types.PropertyGroup):

    parent: bpy.props.BoolProperty(name = "Parent Empty", description = "Add a parent to the empties ", default = False, override = {'LIBRARY_OVERRIDABLE'})
    parent_bone: bpy.props.StringProperty(name = "Parent bone", description = "Parent empty as a parent bone ", override = {'LIBRARY_OVERRIDABLE'})
    parent_object: bpy.props.PointerProperty(name = "Parent object", description = "Parent empty as a parent object ", update = BakeToCtrl.parent_object_update, type = bpy.types.Object, override = {'LIBRARY_OVERRIDABLE'})
    controller: bpy.props.EnumProperty(name = 'Controllers', description="Select empties or a bone with a new rig to bake to", items = [('BONE', 'Bone','Bake to bones','BONE_DATA', 0), ('EMPTY', 'Empty', 'Bake to empties', 'EMPTY_ARROWS', 1)])
    cleanup: bpy.props.EnumProperty(name = 'Clean Up', description="Cleanup created constraints and empties", items = [('ALL', 'All','Remove empties and constraints', 0), ('CONSTRAINTS', 'Constraints', 'Clean all the bone constraints', 1), ('EMPTIES', 'Empties', 'Remove all the baked empties', 2)])

class AnimToolBoxSettings(bpy.types.PropertyGroup):

    # parent: bpy.props.BoolProperty(name = "Parent Empty", description = "Add a parent to the empties ", default = False, override = {'LIBRARY_OVERRIDABLE'})
    # parent_bone: bpy.props.StringProperty(name = "Parent bone", description = "Parent empty as a parent bone ", override = {'LIBRARY_OVERRIDABLE'})
    # parent_object: bpy.props.PointerProperty(name = "Parent object", description = "Parent empty as a parent object ", type = bpy.types.Object, override = {'LIBRARY_OVERRIDABLE'})
    controller: bpy.props.PointerProperty(name = 'Controlled Rig', description="Adding the rig object that is being controlled by the current object", type=bpy.types.Object)
    controlled: bpy.props.PointerProperty(name = 'Controller Rig', description="Adding the rig object that is used as the temp control object", type=bpy.types.Object)
    
    
class ANIMTOOLBOX_PT_Panel:
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Animation"
    #bl_options = {"DEFAULT_CLOSED"}

    @classmethod
    def poll(cls, context):
        return context.object is not None

class ANIMTOOLBOX_PT_MainPanel(ANIMTOOLBOX_PT_Panel, bpy.types.Panel):
    #bl_category = "Animation"
    bl_label = "AnimToolBox"
    bl_idname = "ANIMTOOLBOX_PT_MainPanel"
    bl_options = {"DEFAULT_CLOSED"}
    
    def draw(self, context):
        obj = context.object
        if obj is None:
            return
        # layout = self.layout
        # row = layout.row(align = True)
        
    #     row.label(text = 'Support me on ')     
    #     row.operator("wm.url_open", text="Patreon", icon = 'FUND').url = "https://www.patreon.com/animtoolbox"
    
class BAKETOCTRL_PT_Panel(ANIMTOOLBOX_PT_Panel, bpy.types.Panel):
    #bl_category = "Animation"
    bl_label = "Temp Controls"
    bl_idname = "BAKETOCTRL_PT_Panel"
    bl_parent_id = 'ANIMTOOLBOX_PT_MainPanel'
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        obj = context.object
        if obj is None:
            return
        layout = self.layout
        # row = layout.row(align = True)
        # row.label(text = 'Support me on ')     
        # row.operator("wm.url_open", text="Patreon", icon = 'FUND').url = "https://www.patreon.com/animtoolbox"
        # layout.separator()
        # layout.separator()
        row = layout.row()
        row.prop(context.scene.btc, 'controller', text = '')
        row.label(text = ' Controllers')
        col = layout.column()
        col.operator('anim.bake_to_ctrl', text="Bake Anim to Ctrls", icon = 'OUTLINER_OB_EMPTY')
        row = layout.row()
        row.operator('anim.parent_to_cursor', text="Parent Ctrl on Cursor", icon = 'ORIENTATION_CURSOR')
        #row.operator('anim.parentctrl_switch', text = '', icon = 'CON_ACTION')
        
        #row = layout.row()
        if 'Child_of_Switch' in obj.keys():
            row.prop(obj, '["Child_of_Switch"]', text = 'Parent Influence', slider = True)
        elif obj.animtoolbox.controller is not None:
            if 'Child_of_Switch' in obj.animtoolbox.controller.keys():
                row.prop(obj.animtoolbox.controller, '["Child_of_Switch"]', text = 'Parent Influence', slider = True)
            
        layout.separator()
        row = layout.row()
        row.operator('anim.bake_constrained_bones', text="Quick Bake Constraints", icon = 'REC')

        layout.separator()
        row = layout.row()
        row.operator('anim.select_constrained_bones', text="Select Constrained Bones", icon = 'CONSTRAINT_BONE')
        row = layout.row()
        row.operator('anim.remove_bones_constraints', text="Cleanup", icon = 'TRASH')
        row.prop(context.scene.btc, 'cleanup', text = '')

class ANIMTOOLBOX_PT_Tools(ANIMTOOLBOX_PT_Panel, bpy.types.Panel):
    bl_label = "Anim Tools"
    bl_idname = "ANIMTOOLBOX_PT_Tools"
    bl_parent_id = 'ANIMTOOLBOX_PT_MainPanel'
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        obj = context.object
        if obj is None:
            return
        layout = self.layout
        layout.operator('anim.offset_keyframes', text="Offset Keyframes", icon = 'NEXT_KEYFRAME')
        box = layout.box()
        row = box.row()
        row.label(text = 'Copy World Matrix :', icon = 'WORLD')
        row.operator('anim.copy_matrix', text = '', icon ='DUPLICATE')
        row.operator('anim.paste_matrix', text = '', icon ='PASTEDOWN')
        row = box.row()
        row.label(text = 'Copy Relative Matrix :', icon = 'LINKED')
        row.operator('anim.copy_relative_matrix', text = '', icon ='DUPLICATE')
        row.operator('anim.paste_relative_matrix', text = '', icon ='PASTEDOWN')
        row = layout.row()
        row.operator('anim.share_keyframes', text = 'Share Keyframes', icon ='SNAP_VERTEX')
        
class RIGGERTOOLBOX_PT_Panel(ANIMTOOLBOX_PT_Panel, bpy.types.Panel):
    bl_label = "Rigger Toolbox"
    bl_idname = "RIGGERTOOLBOX_PT_List"
    bl_parent_id = 'ANIMTOOLBOX_PT_MainPanel'
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        obj = context.object
        if obj is None:
            return
        layout = self.layout
        col = layout.column()
        
        col.operator("armature.add_bbone_widgets", text = "Add Bbone Widgets", icon = 'IPO_BEZIER')
        col.separator()
        col.operator("armature.add_chain_ctrls", text = "Add Chain Controls", icon = 'OUTLINER_DATA_ARMATURE')
        col.separator()
        col.operator("armature.merge", text = "Merge Rigs", icon = 'MOD_ARMATURE')

# Add-ons Preferences Update Panel
# Define Panel classes for updating
panels = (ANIMTOOLBOX_PT_MainPanel, BAKETOCTRL_PT_Panel, ANIMTOOLBOX_PT_Tools, RIGGERTOOLBOX_PT_Panel) #anim_layers.ANIMLAYERS_PT_Panel,

def update_panel(self, context):
    message = "AnimationLayers: Updating Panel locations has failed"
    try:
        for panel in panels:
            if "bl_rna" in panel.__dict__:
                bpy.utils.unregister_class(panel)

        for panel in panels:
            #print (panel.bl_category)
            panel.bl_category = context.preferences.addons[__name__].preferences.category
            bpy.utils.register_class(panel)

    except Exception as e:
        print("\n[{}]\n{}\n\nError:\n{}".format(__name__, message, e))
        pass
    
#@addon_updater_ops.make_annotations
class AnimToolBoxPreferences(bpy.types.AddonPreferences):
    # this must match the addon name, use '__package__'
    # when defining this in a submodule of a python package.
    bl_idname = __package__

    category: bpy.props.StringProperty(
        name="Tab Category",
        description="Choose a name for the category of the panel",
        default="Animation",
        update=update_panel
    )
    
    # addon updater preferences from `__init__`, be sure to copy all of them
    auto_check_update: bpy.props.BoolProperty(
        name = "Auto-check for Update",
        description = "If enabled, auto-check for updates using an interval",
        default = False,
    )

    updater_interval_months: bpy.props.IntProperty(
        name='Months',
        description = "Number of months between checking for updates",
        default=0,
        min=0
    )
    updater_interval_days: bpy.props.IntProperty(
        name='Days',
        description = "Number of days between checking for updates",
        default=7,
        min=0,
    
    )
    updater_interval_hours: bpy.props.IntProperty(
        name='Hours',
        description = "Number of hours between checking for updates",
        default=0,
        min=0,
        max=23
    )
    updater_interval_minutes: bpy.props.IntProperty(
        name='Minutes',
        description = "Number of minutes between checking for updates",
        default=0,
        min=0,
        max=59
    )

    def draw(self, context):
        layout = self.layout
        #addon_updater_ops.update_settings_ui(self, context)
        
        row = layout.row()
        col = row.column()

        col.label(text="Tab Category:")
        col.prop(self, "category", text="")

classes = (BakeToCtrlsSettings, ANIMTOOLBOX_PT_MainPanel, BAKETOCTRL_PT_Panel, ANIMTOOLBOX_PT_Tools, AnimToolBoxSettings)
    
    
def register():
    from bpy.utils import register_class
    #addon_updater_ops.register(bl_info)
    register_class(AnimToolBoxPreferences)
    #addon_updater_ops.make_annotations(AnimToolBoxPreferences) # to avoid blender 2.8 warnings
    BakeToCtrl.register()
    Rigger_Toolbox.register()
    for cls in classes:
        register_class(cls)
    Tools.register()
    bpy.types.Scene.btc = bpy.props.PointerProperty(type = BakeToCtrlsSettings)
    bpy.types.Object.animtoolbox = bpy.props.PointerProperty(type = AnimToolBoxSettings)
    update_panel(None, bpy.context)
    #update_tweak_keymap()

def unregister():
    #addon_updater_ops.unregister()
    from bpy.utils import unregister_class
    unregister_class(AnimToolBoxPreferences)
    BakeToCtrl.unregister()
    Rigger_Toolbox.unregister()
    for cls in classes:
        unregister_class(cls)
    Tools.unregister()
    del bpy.types.Scene.btc
    #removing keymaps

if __name__ == "__main__":
    register()