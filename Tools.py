import bpy

from mathutils import Matrix

class Markers_FrameRange(bpy.types.Operator):
    """Create Markers for an interactive frame range"""
    bl_idname = "anim.markers_framerange"
    bl_label = "Markers_Framerange"
    bl_options = {'REGISTER', 'UNDO'}
    
    frame_start: bpy.props.IntProperty(name = 'Frame Start', description="Storing the frame start to check if the property or marker were changed")
    frame_end: bpy.props.IntProperty(name = 'Frame End', description="Storing the frame end to check if the property or marker were changed")
    
    def execute(self, context):
        context.scene.marker_frame_range = False
        context.scene.timeline_markers.remove(self.frame_start_marker)
        context.scene.timeline_markers.remove(self.frame_end_marker)
        return {'FINISHED'}
    
    def invoke(self, context, event):
        #If the modal is already running, then don't run it the second time
        scene = context.scene
        if scene.marker_frame_range:
            scene.marker_frame_range = False
            return {'CANCELLED'}
        
        scene.marker_frame_range = True
        self.frame_start_marker = scene.timeline_markers.new("Frame_Start", frame = scene.frame_start)
        self.frame_end_marker = scene.timeline_markers.new("Frame_End", frame = scene.frame_end)
        self.frame_start_marker.select = False
        self.frame_end_marker.select = False
        self.frame_start = scene.frame_preview_start if scene.use_preview_range else scene.frame_start
        self.frame_end = scene.frame_preview_end if scene.use_preview_range else scene.frame_end
        
        #subscribe_to_frame_range(scene)
        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}
        
    def modal(self, context, event):
        scene = context.scene
        #If one of the markers were removed then remove the rest and quit the modal operator
        if self.frame_start_marker.name not in scene.timeline_markers:
            if self.frame_end_marker.name in scene.timeline_markers:
                scene.timeline_markers.remove(self.frame_end_marker)
            context.scene.marker_frame_range = False
            return {'CANCELLED'}
        if self.frame_end_marker.name not in scene.timeline_markers:
            if self.frame_start_marker.name in scene.timeline_markers:
                scene.timeline_markers.remove(self.frame_start_marker)
            context.scene.marker_frame_range = False
            return {'CANCELLED'}
                
        #If the modal was running again or changed then exit
        if not scene.marker_frame_range:
            self.execute(context)
            return {'FINISHED'}
        
        #get the attribute depending if using preview range or normal frame range
        if scene.use_preview_range:
            frame_start = 'frame_preview_start'
            frame_end = 'frame_preview_end'
        else:
            frame_start = 'frame_start'
            frame_end = 'frame_end'
            
        #update the frame range using marker only if it was changed both from the scene frame range and the operator frame range properties
        if getattr(scene, frame_start) != self.frame_start_marker.frame and self.frame_start_marker.frame != self.frame_start:
            if self.frame_start_marker.frame < 0:
                self.frame_start_marker.frame = 0
            setattr(scene, frame_start, self.frame_start_marker.frame)
            self.frame_start = self.frame_start_marker.frame
            return {'RUNNING_MODAL'}
        elif getattr(scene, frame_start) != self.frame_start:
            self.frame_start_marker.frame = self.frame_start = getattr(scene, frame_start)
            return {'RUNNING_MODAL'}
        
        if getattr(scene, frame_end) != self.frame_end_marker.frame and self.frame_end_marker.frame != self.frame_end:
            setattr(scene, frame_end, self.frame_end_marker.frame)
            self.frame_end = self.frame_end_marker.frame
            return {'RUNNING_MODAL'}
        elif getattr(scene, frame_end) != self.frame_end:
            self.frame_end_marker.frame = self.frame_end = getattr(scene, frame_end)
            return {'RUNNING_MODAL'}
        
        if event.type in {'ESC'}:  # Cancel
            self.execute(context)
            return {'FINISHED'}

        return {'PASS_THROUGH'} 
    
      
def add_offset(obj, offset, bone_id = None):
    if bone_id is None:
        print('bone id is none')
    for fcu in obj.animation_data.action.fcurves:
        if bone_id is not None:
            if bone_id not in fcu.data_path:
                continue
        #print('adding offset of {} to fcu {}'.format(offset, fcu.data_path))
        for keyframe in fcu.keyframe_points:
            keyframe.co[0] += offset
        fcu.update()
        
    
class OffsetKeyframes(bpy.types.Operator):
    """Offset keyframes for every selected object"""
    bl_idname = "anim.offset_keyframes"
    bl_label = "Offset_Keyframes"
    bl_options = {'REGISTER', 'UNDO'}

    frame_offset: bpy.props.FloatProperty(name = 'Frame Offset', description="Add this amount to every empty", default = 1)
    exponential: bpy.props.FloatProperty(name = 'Exponential', description="Add this amount to every offset amount", default = 0)

    @classmethod
    def poll(cls, context):
        return len(context.selected_objects)

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self, width = 125)

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        #row.label(text = 'Offset keyframes ')
        row.prop(self, 'frame_offset')
        #layout.separator()
        row = layout.row()
        row.prop(self, 'exponential')


    def execute(self, context):
        frame_offset = self.frame_offset
        posebones = context.selected_pose_bones
        obj = context.object
        if obj.animation_data is None:
            return {'CANCELLED'}
        if obj.mode =='OBJECT':
            for obj in context.selected_objects:
                add_offset(obj, frame_offset)
                frame_offset += self.frame_offset + self.exponential
                continue
            
        elif obj.mode =='POSE':
            for bone in posebones:
                bone_id = bone.path_from_id()
                print(obj.name,'adding frame offset', frame_offset, bone.name, bone_id)
                add_offset(obj, frame_offset, bone_id)
                frame_offset += self.frame_offset + self.exponential
                
        return {'FINISHED'}

class CopyMatrix(bpy.types.Operator):
    """Copy the matrix of the selection and store it"""
    bl_idname = "anim.copy_matrix"
    bl_label = "Copy"
    bl_options = {'REGISTER', 'UNDO'}
    
    @classmethod
    def poll(cls, context):
        return context.object is not None
    
    def execute(self, context):
        obj = context.object
        global matrix
        
        if obj.mode == 'POSE':
            #bonename = obj.data.bones.active.name
            bone = context.active_pose_bone
            matrix = obj.matrix_world @ bone.matrix.copy()
            
        else:
            matrix = obj.matrix_world.copy()
            
        return {'FINISHED'}

def add_keyframe(bone):
    
    obj = bone.id_data
    
    if obj.animation_data is None:
        return
    if obj.animation_data.action is None:
        return
    
    rotation = 'rotation_quaternion' if bone.rotation_mode == 'QUATERNION' else 'rotation_euler'
    transforms = ['location', rotation, 'scale']
    path =  bone.path_from_id() if obj.mode == 'POSE' else ''
    #find fcurves that are locked to exclude them
    excluded_fcus = {}
    for fcu in obj.animation_data.action.fcurves:
        if path not in fcu.data_path or not fcu.lock:
            continue
        if fcu.data_path in excluded_fcus:
            excluded_fcus[fcu.data_path].append(fcu.array_index)
        else:
            excluded_fcus.update({fcu.data_path : [fcu.array_index]})
        
    
    #insert keyframe only to channels that are not excluded
    for transform in transforms:
        #path = bone.path_from_id() + '.' + transform
        path =  bone.path_from_id() + '.' + transform if obj.mode == 'POSE' else transform
        if path not in excluded_fcus:
            bone.keyframe_insert(transform)
            continue
        length = len(getattr(bone, transform))
        for i in range(length):
            if i in excluded_fcus[path]:
                continue
            bone.keyframe_insert(transform, index = i)
            
        
def is_writable(drv, attr):
    try:
        setattr(drv, attr, getattr(drv, attr))
        return True
    except:
        return False

def rec_drivers_constraints(bone):
    
    obj = bone.id_data
    drivers = []
    constraints = [con for con in bone.constraints if con.enabled]
    path_from_id = bone.path_from_id() + '.' if obj.mode == 'POSE' else ''
    paths = [path_from_id + 'constraints' + '["' + con.name + '"]' + '.enabled' for con in constraints]
    
    if not obj.animation_data:
        return drivers, constraints
    
    # for drv in obj.animation_data.drivers:
    #     if drv.data_path not in paths or drv.mute:
    #         continue
    #     #drv.mute = True
    #     attrs = {}
    #     for attr in dir(drv):
    #         if attr.startswith("__") or not hasattr(drv, attr) or not is_writable(drv, attr):
    #             print(attr, 'is not writable')
    #             continue
    #         print(attr, type(attr))
    #         if drv.is_property_readonly(attr):
    #             print(attr, 'is readonly')
    #         try:
    #             value = getattr(drv, attr)
    #             value = value.copy() if isinstance(value, dict) else value
    #             attrs[attr] = value
    #             print(attr, attrs[attr])
    #         except AttributeError:
    #             pass
        # print(attrs)
        # drivers.append(attrs)
        # obj.animation_data.drivers.remove(drv)
    
    for con in constraints:
        con.mute = True
        
    return drivers, constraints

def restore_drivers_constraints(drivers, constraints):
    #restore constraints and drivers
    for con in constraints:
        con.enabled = True
    for drv in drivers:
        drv.mute = False
    
class PasteMatrix(bpy.types.Operator):
    """paste the matrix of the selection"""
    bl_idname = "anim.paste_matrix"
    bl_label = "Paste"
    bl_options = {'REGISTER', 'UNDO'}
    
    @classmethod
    def poll(cls, context):
        return context.object is not None
    
    def execute(self, context):
        #obj = context.object
        global matrix
        if 'matrix' not in globals():
            return {'CANCELLED'}
        #constraint evaluation is
        # con.target.matrix_world.copy() @ con.inverse_matrix @ basis
        #to flip it I use
        # con.inverse_matrix.inverted() @ con.target.matrix_world.copy().inverted() @ matrix
        # to find the basis (Matrix.Identity(4) + con.target.matrix_world @ con.inverse_matrix).inverted() @ (2 * matrix)
        
        #finding the influence
        # using for location and rotation obj.matrix_basis = (Matrix.Identity(4) + 0.3 * (con.target.matrix_world @ con.inverse_matrix - Matrix.Identity(4))).inverted() @ matrix
        # using for scale obj.matrix_basis = (Matrix.Identity(4).lerp(offset, 0.7)).inverted() @ matrix
        #formula for the rest of the constraints
        # diff = basis.inverted() @ obj.matrix_parent_inverse.inverted() @ obj.parent.matrix_world.inverted() @ matrix_world 
        # obj.matrix_basis = obj.matrix_parent_inverse.inverted() @ obj.parent.matrix_world.inverted() @ matrix @ diff.inverted()
        
        #apply the new matrix with the difference
        for obj in context.selected_objects:
            if obj.mode == 'POSE':
                for target in context.selected_pose_bones:
                    # bonename = obj.data.bones.active.name
                    # target = obj.pose.bones[bonename]
                    matrix_basis = inverse_constraint_offset(target, matrix)
                    target.matrix = matrix_basis
            else:
                target = obj
                #drivers, constraints = rec_drivers_constraints(target)
                matrix_basis = inverse_constraint_offset(target, matrix)
                target.matrix_world = matrix_basis
                
                # for drv_attr in drivers:
                #     drv = obj.animation_data.drivers.new(drv_attr['data_path'])
                #     for key, value in drv_attr.items():
                #         setattr(drv, key, value)
                
            #if autokey is turned on then add a keyframe
            if scene.tool_settings.use_keyframe_insert_auto and 'target' in locals():
                add_keyframe(target)
           
        return {'FINISHED'}
              
class CopyRelativeMatrix(bpy.types.Operator):
    """Copy the relative distance matrix from active between two objects or bones"""
    bl_idname = "anim.copy_relative_matrix"
    bl_label = "Copy_Relative_Matrix"
    bl_options = {'REGISTER', 'UNDO'}
    
    @classmethod
    def poll(cls, context):
        return context.object is not None
    
    def execute(self, context):
        obj = context.object
        global matrix_dist, source_active_name, source_rig_name
        
        if obj.mode == 'POSE':
            if len(context.selected_pose_bones) < 2:
                return {'CANCELLED'}
            source_active = context.active_pose_bone
            source_rig_name = obj.name
            for bone_relative in context.selected_pose_bones:
                if bone_relative != source_active:
                    break
            
            matrix_dist = source_active.matrix.inverted() @ bone_relative.matrix# @ obj.matrix_world
            
        else:
            if len(context.selected_objects)  < 2:
                #bpy.ops.wm.popup_menu_invoke(self, menu = "INFO_MT_add", title= 'Select at least two objects')
                return {'CANCELLED'}
            source_active = context.active_object
            for obj_relative in context.selected_objects:
                if obj_relative != source_active:
                    break
                
            matrix_dist = source_active.matrix_world.inverted() @ obj_relative.matrix_world
            #if the source rig exist from previouslz
            if 'source_rig_name' in globals():
                del source_rig_name
            
            
        source_active_name = source_active.name   
            
        return {'FINISHED'}

def inverse_constraint_offset(source, matrix_source):
    
    offsets = []
    offsets_lerp = []
    offset_inv = Matrix.Identity(4)
    offset_inv_lerp = Matrix.Identity(4)
    obj = source.id_data
    
    #iterate and store all the inverted offsets of all the child constraints
    for con in source.constraints:
        if con.type != 'CHILD_OF' or con.mute or not con.influence:
            continue
        parent_matrix = con.target.matrix_world
        if con.subtarget != '':
            parent_matrix = con.target.pose.bones[con.subtarget].matrix
        offsets.append(Matrix.Identity(4) + con.influence * (parent_matrix @ con.inverse_matrix - Matrix.Identity(4)))
        offsets_lerp.append(Matrix.Identity(4).lerp(parent_matrix @ con.inverse_matrix, con.influence))
    
    #Multiply all the child constraint inverted offsets
    for offset, offsets_lerp in zip(offsets, offsets_lerp):
        #offset_inv = offset_inv @ parent_offset_inv
        offset_inv = offset_inv @ offset.inverted()
        offset_inv_lerp = offset_inv_lerp @ offsets_lerp.inverted()
    #add object space if it's a bone    
    if source != obj:
        offset_inv = offset_inv @ obj.matrix_world.inverted()
        
    #final Matrix values
    matrix_basis =  offset_inv @ matrix_source
    matrix_lerp = offset_inv_lerp @ matrix_source
    loc, rot, scale = matrix_basis.decompose()
    loc_lerp, rot_lerp, scale_lerp = matrix_lerp.decompose()
    
    matrix_basis = Matrix.LocRotScale(loc, rot_lerp, scale_lerp)
    
    #print(matrix_basis)
    
    return matrix_basis

def redraw_areas(areas):
    for area in bpy.context.window_manager.windows[0].screen.areas:
        if area.type in areas:
            area.tag_redraw()
            
class PasteRelativeMatrix(bpy.types.Operator):
    """paste the relative matrix of the selection"""
    bl_idname = "anim.paste_relative_matrix"
    bl_label = "Paste_Relative"
    bl_options = {'REGISTER', 'UNDO'}
    
    @classmethod
    def poll(cls, context):
        return context.object is not None
    
    def execute(self, context):
        obj = context.object
        global matrix_dist, source_active_name, source_rig_name
        if 'matrix_dist' not in globals():
            return {'CANCELLED'}

        for obj in context.selected_objects:
            if obj.mode == 'POSE':
                for target in context.selected_pose_bones:
                    #target = context.active_pose_bone
                    source_rig = bpy.data.objects[source_rig_name]
                    #check that the selected bone is not the source bone
                    if target == source_rig.pose.bones[source_active_name]:
                        return {'CANCELLED'}
                    #Get the current matrix of the source bone
                    matrix_source = source_rig.pose.bones[source_active_name].matrix
                    
                    matrix_new = matrix_source @ matrix_dist
                    #print(matrix_new)
                    matrix_basis = inverse_constraint_offset(target, matrix_new)  
                    #print('basis matrix ', matrix_basis) 
                    target.matrix = obj.matrix_world @ matrix_basis
                    #print('target matrix ', target.matrix)
                    #redraw_areas('VIEW_3D')
                     
            else:
                target = obj
                #check that the selected obj is not the source object
                if source_active_name in bpy.data.objects:
                    if target == bpy.data.objects[source_active_name]:
                        return {'CANCELLED'}
                    #get the new matrix of the source object
                    matrix_source = bpy.data.objects[source_active_name].matrix_world
                elif 'source_rig_name' in globals():
                    if source_rig_name in bpy.data.objects:
                        matrix_source = bpy.data.objects[source_rig_name].pose.bones[source_active_name].matrix
                else:
                    return {'CANCELLED'}

                matrix_new = matrix_source @ matrix_dist 
                matrix_basis = inverse_constraint_offset(target, matrix_new)   
                target.matrix_world = matrix_basis
                
         
            #if autokey is turned on then add a keyframe
            if scene.tool_settings.use_keyframe_insert_auto:
                add_keyframe(target)
           
        return {'FINISHED'}

def get_fcurves_keyframes(selection, fcurves, frames, group):
    obj = selection.id_data
    group = selection.name 
    #get all the transformations that will share the keys
    if selection.rotation_mode == 'QUATERNION':
        rot = ['rotation_quaternion'] * 4
    elif selection.rotation_mode == 'AXIS_ANGLE':
        rot = ['rotation_axis_angle'] * 4
    else:
        rot = ['rotation_euler']*3    
    transformations = [['location']*3, rot, ['scale']*3]
    for transform in transformations:
        for i, transform_name in enumerate(transform):
            try:
                path = selection.path_from_id() + '.' + transform_name
            except ValueError:
                path = transform_name
            #check if it has any fcurves, if not then create them
            fcu = obj.animation_data.action.fcurves.find(path, index = i)
            if fcu is None:
                #create a new fcurve 
                fcu = obj.animation_data.action.fcurves.new(data_path = path, index = i, action_group = group)
            #if an fcurve group already exists then use it instead
            elif fcu.group is not None:
                group = fcu.group.name
            #add the fcurve and also the current value of the selection as a dictionary value
            fcurves.update({fcu : getattr(selection, transform_name)[i]})
            
            #store all the frame numbers with keyframes on selected object or bones
            for keyframe in fcu.keyframe_points:
                if keyframe.co[0] not in frames:
                    frames.add(keyframe.co[0])
                    
    return fcurves, frames
    
class ShareKeys(bpy.types.Operator):
    """Share keyframes between all the selected objects and bones"""
    bl_idname = "anim.share_keyframes"
    bl_label = "Share_Keyframes"
    bl_options = {'REGISTER', 'UNDO'}
    
    @classmethod
    def poll(cls, context):
        return context.object is not None
    
    def execute(self, context):
        #obj = context.object
        fcurves = dict()
        frames = set()
        if context.object.mode == 'POSE':
            if context.selected_pose_bones:
                for posebone in context.selected_pose_bones:
                    fcurves, frames = get_fcurves_keyframes(posebone, fcurves, frames, posebone.name)
        else:
            for obj in context.selected_objects:
                #if there is no animation data or an action then create it
                if obj.animation_data is None:
                    obj.animation_data_create()
                if obj.animation_data.action is None:
                    action = bpy.data.actions.new(obj.name)
                    obj.animation_data.action = action
                    
                fcurves, frames = get_fcurves_keyframes(obj, fcurves, frames, 'Object Transforms')
        

            # #check if it has any fcurves, if not then create one
            # if posebone.rotation_mode == 'QUATERNION':
            #     rot = ['rotation_quaternion'] * 4
            # elif posebone.rotation_mode == 'AXIS_ANGLE':
            #     rot = ['rotation_axis_angle'] * 4
            # else:
            #     rot = ['rotation_euler']*3    
            # transformations = [['location']*3, rot, ['scale']*3]
            # for transform in transformations:
            #     for i, transform_name in enumerate(transform):
            #         path = posebone.path_from_id() + '.' + transform_name
                    
            #         fcu = obj.animation_data.action.fcurves.find(path, index = i)
            #         if fcu is None:
            #             #create a new fcurve 
            #             fcu = obj.animation_data.action.fcurves.new(data_path = path, index = i, action_group = group)
            #         #if an fcurve group already exists then use it instead
            #         elif fcu.group is not None:
            #             group = fcu.group.name
            #         #add the fcurve and also the current value of the selection as a dictionary value
            #         fcurves.update({fcu : getattr(posebone, transform_name)[i]})
                    
            #         #store all the frame numbers with keyframes on selected object or bones
            #         for keyframe in fcu.keyframe_points:
            #             if keyframe.co[0] not in frames:
            #                 frames.add(keyframe.co[0])
                        
        #write all the keyframes
        for fcu, current_value in fcurves.items():
            for frame in frames:
                if len(fcu.keyframe_points):
                    skip = False
                    for keyframe in fcu.keyframe_points:
                        if keyframe.co[0] == frame:
                            skip = True
                    #if a keyframe already exists in the fcurve at this frame then skip
                    if skip:
                        continue
                    value = fcu.evaluate(frame)
                else:
                    value = current_value
                fcu.keyframe_points.add(1)
                fcu.keyframe_points[-1].co = (frame, value)
                fcu.update()
                    
        return {'FINISHED'}
                
                
def draw_func(self, context):
    layout = self.layout

    # Append your label to the frame range template ID
    layout.operator("anim.markers_framerange", icon = 'MARKER', text ='', depress = context.scene.marker_frame_range)
        
classes = (OffsetKeyframes, CopyMatrix, PasteMatrix, CopyRelativeMatrix, PasteRelativeMatrix, ShareKeys, Markers_FrameRange)

def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)
    bpy.types.Scene.marker_frame_range = bpy.props.BoolProperty(name = "Marker Frame Range", description = "Flag when marker frame range turned on", default = False, override = {'LIBRARY_OVERRIDABLE'})
    bpy.types.DOPESHEET_HT_header.append(draw_func)
    
def unregister():
    from bpy.utils import unregister_class
    for cls in classes:
        unregister_class(cls)
    del bpy.types.Scene.marker_frame_range
    bpy.types.DOPESHEET_HT_header.remove(draw_func)
