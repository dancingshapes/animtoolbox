# ***** BEGIN GPL LICENSE BLOCK *****
#
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ***** END GPL LICENCE BLOCK *****


import bpy
#from . import Rigger_Toolbox
from mathutils import Matrix
#import mathutils
import numpy as np

def draw_wgt(boneLength, bone, shape = 'sphere'):
    if shape == 'BONE': 
        return None
    if shape == 'ORIGINAL':
        return bone.custom_shape
    suffix = bone.id_data.name + '_' + bone.name
    if 'WGTB_object' + suffix in bpy.data.objects:
        obj = bpy.data.objects['WGTB_object'] + suffix
        if 'WGTB_shape' + suffix in obj.data.name:
            return obj
    mesh = bpy.data.meshes.new('WGTB_shape_' + suffix)
    obj = bpy.data.objects.new('WGTB_object_' + suffix, mesh)
    
    #coordinates of the different widget shape
    arrows = {'vertices': [(0.0, 0.0, -1.1981923897508295e-08), (0.0, 0.0, 0.40204665064811707), (0.0, 0.40204665064811707, -1.1981923897508295e-08), (0.40204665064811707, 0.0, -1.1981923897508295e-08), (-0.008419156074523926, 0.008419156074523926, 0.39362743496894836), (-0.008419156074523926, 0.008419156074523926, 0.4104657769203186), (-0.008419185876846313, -0.008419156074523926, 0.39362743496894836), (-0.008419156074523926, -0.008419156074523926, 0.4104657769203186), (0.008419156074523926, 0.008419156074523926, 0.39362743496894836), (0.008419156074523926, 0.008419156074523926, 0.4104657769203186), (0.008419156074523926, -0.008419156074523926, 0.39362743496894836), (0.008419156074523926, -0.008419156074523926, 0.4104657769203186), (0.39362743496894836, 0.008419156074523926, -0.008419156074523926), (0.39362743496894836, 0.008419156074523926, 0.008419156074523926), (0.39362743496894836, -0.008419156074523926, -0.008419156074523926), (0.39362743496894836, -0.008419156074523926, 0.008419156074523926), (0.4104657769203186, 0.008419156074523926, -0.008419156074523926), (0.4104657769203186, 0.008419156074523926, 0.008419156074523926), (0.4104657769203186, -0.008419156074523926, -0.008419156074523926), (0.4104657769203186, -0.008419156074523926, 0.008419156074523926), (-0.008419156074523926, 0.4104657769203186, -0.008419167250394821), (-0.008419156074523926, 0.4104657769203186, 0.008419143967330456), (-0.008419156074523926, 0.39362743496894836, -0.008419167250394821), (-0.008419156074523926, 0.39362743496894836, 0.008419143967330456), (0.008419156074523926, 0.4104657769203186, -0.008419167250394821), (0.008419156074523926, 0.4104657769203186, 0.008419143967330456), (0.008419156074523926, 0.39362743496894836, -0.008419167250394821), (0.008419156074523926, 0.39362743496894836, 0.008419143967330456)], 'edges': [[2, 0], [0, 1], [0, 3], [6, 4], [4, 5], [5, 7], [7, 6], [10, 6], [7, 11], [11, 10], [8, 10], [11, 9], [9, 8], [4, 8], [9, 5], [14, 12], [12, 13], [13, 15], [15, 14], [18, 14], [15, 19], [19, 18], [16, 18], [19, 17], [17, 16], [12, 16], [17, 13], [22, 20], [20, 21], [21, 23], [23, 22], [26, 22], [23, 27], [27, 26], [24, 26], [27, 25], [25, 24], [20, 24], [25, 21]], 'faces': []}
    plain_axes = {'vertices': [(0.0, 0.0, 0.40140801668167114), (0.0, 0.0, -0.40140801668167114), (0.40140801668167114, 0.0, 0.0), (-0.40140801668167114, 0.0, 0.0), (0.0, -0.40140801668167114, 0.0), (0.0, 0.40140801668167114, 0.0), (0.0, 0.0, 0.0)], 'edges': [[6, 5], [4, 6], [6, 1], [6, 3], [0, 6], [2, 6]], 'faces': []}
    single_arrow = {'vertices': [(0.0, 1.3104262563956581e-08, 0.2997905910015106), (0.0, 0.0, 0.0), (0.011538410559296608, 0.013322020880877972, 0.29990124702453613), (0.011538410559296608, -0.013321994803845882, 0.29990124702453613), (0.0, 1.7448547495746425e-08, 0.3991762399673462), (-0.011538410559296608, 0.013322020880877972, 0.29990124702453613), (-0.011538410559296608, -0.013321994803845882, 0.29990124702453613)], 'edges': [[0, 1], [2, 4], [2, 3], [5, 2], [3, 6], [5, 6], [6, 4], [4, 5], [4, 3]], 'faces': []}
    circle = {'vertices': [(1.3969838619232178e-09, -1.7606295088512525e-08, -0.4027850925922394), (-0.1541391760110855, -1.6266094249317575e-08, -0.37212488055229187), (-0.2848120629787445, -1.2449531183733598e-08, -0.2848120629787445), (-0.37212488055229187, -6.737637558984488e-09, -0.1541391760110855), (-0.4027850925922394, 5.551115123125783e-17, 0.0), (-0.37212488055229187, 6.737637558984488e-09, 0.1541391760110855), (-0.2848120629787445, 1.2449531183733598e-08, 0.2848120629787445), (-0.1541391760110855, 1.6266094249317575e-08, 0.37212488055229187), (1.3969838619232178e-09, 1.7606295088512525e-08, 0.4027850925922394), (0.1541391760110855, 1.6266094249317575e-08, 0.37212488055229187), (0.2848120629787445, 1.2449531183733598e-08, 0.2848120629787445), (0.37212488055229187, 6.737637558984488e-09, 0.1541391760110855), (0.4027850925922394, 5.551115123125783e-17, 0.0), (0.37212488055229187, -6.737637558984488e-09, -0.1541391760110855), (0.2848120629787445, -1.2449531183733598e-08, -0.2848120629787445), (0.1541391760110855, -1.6266094249317575e-08, -0.37212488055229187)], 'edges': [[1, 0], [2, 1], [3, 2], [4, 3], [5, 4], [6, 5], [7, 6], [8, 7], [9, 8], [10, 9], [11, 10], [12, 11], [13, 12], [14, 13], [15, 14], [0, 15]], 'faces': []}
    cube = {'vertices': [(-0.4013901352882385, -0.4013901352882385, -0.4013901352882385), (-0.4013901352882385, -0.4013901352882385, 0.4013901352882385), (-0.4013901352882385, 0.4013901352882385, 0.4013901352882385), (-0.4013901352882385, 0.4013901352882385, -0.4013901352882385), (0.4013901352882385, -0.4013901352882385, -0.4013901352882385), (0.4013901352882385, -0.4013901352882385, 0.4013901352882385), (0.4013901352882385, 0.4013901352882385, 0.4013901352882385), (0.4013901352882385, 0.4013901352882385, -0.4013901352882385)], 'edges': [[0, 1], [1, 2], [2, 3], [3, 0], [4, 5], [5, 6], [6, 7], [7, 4], [0, 4], [1, 5], [2, 6], [3, 7]], 'faces': []}
    sphere = {'vertices': [(0.0, 0.40137654542922974, 0.0), (-0.10388384014368057, 0.3876999318599701, 0.0), (-0.20068827271461487, 0.34760206937789917, 0.0), (-0.2838158905506134, 0.2838158905506134, 0.0), (-0.34760230779647827, 0.20068813860416412, 0.0), (-0.3876999318599701, 0.10388384014368057, 0.0), (-0.40137654542922974, 3.030309159157696e-08, 0.0), (-0.3876999318599701, -0.10388379544019699, 0.0), (-0.34760230779647827, -0.20068813860416412, 0.0), (-0.2838161289691925, -0.2838158905506134, 0.0), (-0.20068827271461487, -0.34760206937789917, 0.0), (-0.10388395935297012, -0.3876999318599701, 0.0), (-1.5630175198566576e-07, -0.40137654542922974, 0.0), (0.10388367623090744, -0.3876999318599701, 0.0), (0.20068803429603577, -0.34760230779647827, 0.0), (0.2838158905506134, -0.2838161289691925, 0.0), (0.34760206937789917, -0.20068837702274323, 0.0), (0.387699693441391, -0.10388403385877609, 0.0), (0.40137654542922974, -1.8660485068267008e-07, 0.0), (0.3876999318599701, 0.10388367623090744, 0.0), (0.34760230779647827, 0.20068803429603577, 0.0), (0.2838161289691925, 0.2838158905506134, 0.0), (0.20068851113319397, 0.34760206937789917, 0.0), (0.10388415306806564, 0.387699693441391, 0.0), (0.0, 2.990487502074757e-08, 0.40137654542922974), (-0.10388384014368057, 2.990487502074757e-08, 0.3876999318599701), (-0.20068827271461487, 2.990487502074757e-08, 0.34760206937789917), (-0.2838158905506134, 2.990487502074757e-08, 0.2838158905506134), (-0.34760230779647827, 1.4952437510373784e-08, 0.20068813860416412), (-0.3876999318599701, 7.476218755186892e-09, 0.10388384014368057), (-0.40137654542922974, 3.564938905328222e-15, 3.030309159157696e-08), (-0.3876999318599701, -7.476218755186892e-09, -0.10388379544019699), (-0.34760230779647827, -1.4952437510373784e-08, -0.20068813860416412), (-0.2838161289691925, -2.990487502074757e-08, -0.2838158905506134), (-0.20068827271461487, -2.990487502074757e-08, -0.34760206937789917), (-0.10388395935297012, -2.990487502074757e-08, -0.3876999318599701), (-1.5630175198566576e-07, -2.990487502074757e-08, -0.40137654542922974), (0.10388367623090744, -2.990487502074757e-08, -0.3876999318599701), (0.20068803429603577, -2.990487502074757e-08, -0.34760230779647827), (0.2838158905506134, -2.990487502074757e-08, -0.2838161289691925), (0.34760206937789917, -1.4952437510373784e-08, -0.20068837702274323), (0.387699693441391, -7.476218755186892e-09, -0.10388403385877609), (0.40137654542922974, -1.425975562131289e-14, -1.8660485068267008e-07), (0.3876999318599701, 7.476218755186892e-09, 0.10388367623090744), (0.34760230779647827, 1.4952437510373784e-08, 0.20068803429603577), (0.2838161289691925, 2.990487502074757e-08, 0.2838158905506134), (0.20068851113319397, 2.990487502074757e-08, 0.34760206937789917), (0.10388415306806564, 2.990487502074757e-08, 0.387699693441391), (-2.990487502074757e-08, 1.782469452664111e-15, 0.40137654542922974), (-3.738109199957762e-08, -0.10388384014368057, 0.3876999318599701), (-4.485731253112135e-08, -0.20068827271461487, 0.34760206937789917), (-5.980975004149514e-08, -0.2838158905506134, 0.2838158905506134), (-2.990487502074757e-08, -0.34760230779647827, 0.20068813860416412), (-2.990487502074757e-08, -0.3876999318599701, 0.10388384014368057), (-2.990487502074757e-08, -0.40137654542922974, 3.030309159157696e-08), (-2.990487502074757e-08, -0.3876999318599701, -0.10388379544019699), (0.0, -0.34760230779647827, -0.20068813860416412), (0.0, -0.2838161289691925, -0.2838158905506134), (1.4952437510373784e-08, -0.20068827271461487, -0.34760206937789917), (2.2428656265560676e-08, -0.10388395935297012, -0.3876999318599701), (2.990486080989285e-08, -1.5630175198566576e-07, -0.40137654542922974), (3.738109199957762e-08, 0.10388367623090744, -0.3876999318599701), (4.485731253112135e-08, 0.20068803429603577, -0.34760230779647827), (5.980975004149514e-08, 0.2838158905506134, -0.2838161289691925), (2.990487502074757e-08, 0.34760206937789917, -0.20068837702274323), (2.990487502074757e-08, 0.387699693441391, -0.10388403385877609), (2.990487502074757e-08, 0.40137654542922974, -1.8660485068267008e-07), (2.990487502074757e-08, 0.3876999318599701, 0.10388367623090744), (0.0, 0.34760230779647827, 0.20068803429603577), (0.0, 0.2838161289691925, 0.2838158905506134), (-1.4952437510373784e-08, 0.20068851113319397, 0.34760206937789917), (-2.2428656265560676e-08, 0.10388415306806564, 0.387699693441391), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0)], 'edges': [[0, 1], [1, 2], [2, 3], [3, 4], [4, 5], [5, 6], [6, 7], [7, 8], [8, 9], [9, 10], [10, 11], [11, 12], [12, 13], [13, 14], [14, 15], [15, 16], [16, 17], [17, 18], [18, 19], [19, 20], [20, 21], [21, 22], [22, 23], [0, 23], [24, 25], [25, 26], [26, 27], [27, 28], [28, 29], [29, 30], [30, 31], [31, 32], [32, 33], [33, 34], [34, 35], [35, 36], [36, 37], [37, 38], [38, 39], [39, 40], [40, 41], [41, 42], [42, 43], [43, 44], [44, 45], [45, 46], [46, 47], [24, 47], [48, 49], [49, 50], [50, 51], [51, 52], [52, 53], [53, 54], [54, 55], [55, 56], [56, 57], [57, 58], [58, 59], [59, 60], [60, 61], [61, 62], [62, 63], [63, 64], [64, 65], [65, 66], [66, 67], [67, 68], [68, 69], [69, 70], [70, 71], [48, 71]], 'faces': []}
    cone = {'vertices': [(-3.0009825735533013e-09, 0.402785062789917, 1.7763568394002505e-15), (0.2848120331764221, 0.2848120331764221, 0.0), (0.402785062789917, 0.0, 0.0), (0.2848120331764221, -0.2848120331764221, 0.0), (-3.0009825735533013e-09, -0.402785062789917, -1.7763568394002505e-15), (-0.2848120331764221, -0.2848120331764221, 0.0), (-0.402785062789917, 0.0, 0.0), (-0.2848120331764221, 0.2848120331764221, 0.0), (-3.0009825735533013e-09, -3.552713678800501e-15, 0.805570125579834)], 'edges': [[0, 1], [0, 8], [8, 1], [1, 2], [8, 2], [2, 3], [8, 3], [3, 4], [8, 4], [4, 5], [8, 5], [5, 6], [8, 6], [6, 7], [8, 7], [7, 0]], 'faces': []}

    shapes = {'arrows':arrows, 'plain_axes':plain_axes,'single_arrow':single_arrow, 'cube':cube, 'sphere' : sphere , 'circle' : circle, 'cone' : cone}
    shape = shape.lower()
    mesh.from_pydata(np.array(shapes[shape]['vertices'])*[boneLength, boneLength, boneLength] , shapes[shape]['edges'], shapes[shape]['faces'])
    obj['bate'] = True
    return obj

def original_shape_properties(ctrl, posebone, size):
    '''copy all the properties from the original bone shape'''
    for prop in dir(posebone):
        #iterate only over custom shape properties
        if 'custom_shape' not in prop:
            continue
        if not hasattr(ctrl, prop):
            continue
        value = getattr(posebone, prop)
        if 'scale' in prop:
            value = tuple(value * size)
        setattr(ctrl, prop, value)

def constraint_add(source, target, con_type = 'COPY_TRANSFORMS'):
    '''Add a constraint with the source and target, available both to bones and empties'''
    constraint = source.constraints.new(con_type)
    if hasattr(target, 'bone'):
        constraint.target = target.id_data
        constraint.subtarget = target.name
    else:
        constraint.target = target
        
    #Add name suffix if it's not on a control rig object
    if 'bate' not in constraint.id_data.keys():
        constraint.name = con_type.capitalize() + '_to_Ctrl'

    return constraint

def constraint_ctrl_to_bone(ctrl, bone, bake_loc, con_type, constraints):
    constraint = constraint_add(ctrl, bone, con_type)
    if bake_loc == 'TAIL':
        constraint.head_tail = 1
    constraints.append(constraint)
    
    return constraints

def add_rig_bone(rig, source):
    
    
    bpy.context.view_layer.objects.active = rig
    bpy.ops.object.mode_set(mode = 'EDIT')
    #add a bone
    bone = rig.data.edit_bones.new('Parent_bone')
    
    if hasattr(source, 'matrix'):
        bone.matrix = source.matrix
    # elif hasattr(source, 'matrix_world'):
    #     bone.matrix_world = source.matrix_world
    bone.length = 1
    bonename = bone.name
    
    bpy.ops.object.mode_set(mode = 'POSE')
    posebone = rig.pose.bones[bonename]
    posebone.rotation_mode = 'XYZ'
    return posebone

def create_collection(obj):
    #create a collection
    if 'Ctrl_Helpers' not in bpy.data.collections:
        collection = bpy.data.collections.new('Ctrl_Helpers')
        bpy.context.scene.collection.children.link(collection)
    else:
        collection = bpy.data.collections['Ctrl_Helpers']
    if obj not in collection.objects.values():
        collection.objects.link(obj)
        
    return collection

def add_empty(self, bone):
    empty = bpy.data.objects.new(bone.name, None)
    #display properties
    empty.empty_display_size = self.display_size
    empty.empty_display_type = self.display_type
    empty['bate'] = True
    
    if bone is None:
        empty.matrix_world = Matrix()
        return empty

    #for a bone assign matrix, for empty matrix world
    if hasattr(bone, 'matrix'):
        empty.matrix_world = bone.matrix.to_4x4()
    elif hasattr(bone, 'matrix_world'):
        empty.matrix_world = bone.matrix_world.to_4x4()
        
    create_collection(empty)    
    return empty
       
def add_control_empties(self, bones):
    #Add an empty to the scene
    btc = bpy.context.scene.btc
    empties = []
    if btc.parent:
        if btc.parent_object.type == 'ARMATURE':
            parent = btc.parent_object.pose.bones[btc.parent_bone]
        else:
            parent = btc.parent_object
        parent_empty = add_empty(self, parent)
        parent_empty.empty_display_size *= 2
    else:
        parent_empty = None
        
    for bone in bones:
        empty = add_empty(self, bone)
        if parent_empty:
            empty.parent = parent_empty
            #empty.matrix_parent_inverse = parent_empty.matrix_world.inverted()
        if self.child:
            child_empty = add_empty(self, bone)
            child_empty.parent = empty
            child_empty.matrix_parent_inverse = empty.matrix_world.inverted()
            empty.empty_display_size *= 0.5
        empties.append(empty)
    return empties

def add_ctrl_bone(rig, bone, parent_bone, nameextension = 'Ctrl_', bake_loc = 'HEAD', length_factor = 1):
    ctrl_bone = rig.data.edit_bones.new(nameextension + bone.name)
    ctrl_bone.parent = parent_bone
    if bone is None:
        ctrl_bone.matrix_world = Matrix()

    if hasattr(bone, 'matrix'):
        matrix = bone.matrix
    elif hasattr(bone, 'matrix_world'):
        matrix = bone.matrix_world
        
    #decompose and compose a new matrix at the tail location    
    if bake_loc == 'TAIL':
        loc, rot, scale = matrix.decompose()
        loc = bone.tail
        matrix = Matrix.LocRotScale(loc, rot, scale)
        
    ctrl_bone.matrix = matrix
            
    ctrl_bone.length = bone.length * length_factor
    
    return ctrl_bone

def add_ctrl_rig(bones):
    '''Adding the control rig bones'''
    org_obj = bones[0].id_data
    rigname = 'Controller_' + org_obj.name

    #check if the rig object exists, if not add a new one
    if rigname not in bpy.data.objects:
        armature = bpy.data.armatures.new('Controller_' + org_obj.data.name)
        rig = bpy.data.objects.new(rigname, object_data = armature)
    else:
        rig = bpy.data.objects[rigname]
        
    create_collection(rig)
    #stamp the rig with the addon module stamp
    rig['bate'] = True
    rig.animtoolbox.controlled = org_obj
    org_obj.animtoolbox.controller = rig
    bpy.context.view_layer.objects.active = rig
    
    return rig

def add_parent_bone(rig):
    scene = bpy.context.scene
    btc = scene.btc
    #return only the parent bone, otherwise it will return empty
    parent_bone = None
    if not btc.parent:
        return None
    
    #check if the parent bone is already in the rig, and if yes then return
    if btc.parent_bone in rig.data.edit_bones:
        parent_bone = rig.data.edit_bones[btc.parent_bone]
        parent_bone.select = True
        return parent_bone
    
    #add a new parent bone
    parent_bone = rig.data.edit_bones.new(btc.parent_bone)
    parent_bone.select = True
    
    #get the matrix from the original bone that is used as a parent
    if btc.parent_bone:
        parent_bone.head = btc.parent_object.data.edit_bones[btc.parent_bone].head
        parent_bone.tail = btc.parent_object.data.edit_bones[btc.parent_bone].tail
        parent_bone.roll = btc.parent_object.data.edit_bones[btc.parent_bone].roll
        parent_bone.matrix = btc.parent_object.data.edit_bones[btc.parent_bone].matrix
        parent_bone.length = btc.parent_object.data.edit_bones[btc.parent_bone].length*2
        #parent = parent_bone
    elif btc.parent_object:
        parent_bone.matrix = btc.parent_object.matrix_world
        parent_bone.length = 1
    #if There's no object applied as a reference for the parent then use the cursor
    else:
        parent_bone.matrix = scene.cursor.matrix
        parent_bone.length = 1
        #parent_bone.length = btc.parent_object.data.edit_bones[btc.parent_bone].length*2
        
    return parent_bone
    
def add_control_bones(self, bones, rig):
    '''Adding all the control bones, ctrl childs and override transform bones during edit mode'''
    # obj_mode =  bones[0].id_data.mode
    # print('current object', bpy.context.active_object.name)
    for bone in bones:
        bone.id_data.data.bones[bone.name].select = False
    #deselect the original rig in case it's linked
    #bones[0].id_data.select_set(False)
    bpy.ops.object.mode_set(mode = 'EDIT')
    ctrls = []
    child_ctrls = []
    ot_ctrls = [] #list of override transforms
    
    parent_bone = add_parent_bone(rig)
    for bone in bones:
        ctrl_bone = add_ctrl_bone(rig, bone, parent_bone, 'Ctrl_', self.bake_loc)
        ctrls.append(ctrl_bone.name)

        #add bone in case it's using transform from another bone
        if self.display_type == 'ORIGINAL' and bone.custom_shape_transform is not None:
            #boneshape = bone.custom_shape_transform
            boneshape = add_ctrl_bone(rig, bone.custom_shape_transform, parent_bone, '', self.bake_loc)
            ot_ctrls.append(boneshape.name)
            
        #add child controls
        if self.child:
            child_ctrl = add_ctrl_bone(rig, bone, ctrl_bone, 'Child_ctrl_', self.bake_loc, 0.5)
            child_ctrls.append(child_ctrl)
        ctrl_bone.select = True

    
    return ctrls, parent_bone

def add_ctrl_group(rig):
    #Add the bone group if doesn't exist
    bone_groups = rig.pose.bone_groups
    if 'Ctrl Bones' not in bone_groups:
        ctrl_group = bone_groups.new(name = 'Ctrl Bones')
        ctrl_group.color_set = 'THEME09'
    else:
        ctrl_group = bone_groups['Ctrl Bones']
        
    return ctrl_group
    

class SelectConstrainedBones(bpy.types.Operator):
    """Select all the bones that are constrained to the empty"""
    bl_idname = "anim.select_constrained_bones"
    bl_label = "Select_Constrained_Bones"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.object.type == 'ARMATURE'

    def execute(self, context):
        obj = context.object
        if obj.mode != 'POSE':
            return {'CANCELLED'}
        for bone in obj.data.bones:
            for con in obj.pose.bones[bone.name].constraints:
                if not '_to_Ctrl' in con.name:
                    continue
                bone.select = True

        return {'FINISHED'}

class BakeConstraints(bpy.types.Operator):
    """Bake all the bones with constraints to the empties"""
    bl_idname = "anim.bake_constrained_bones"
    bl_label = "Quick Bake Constraints"
    bl_options = {'REGISTER', 'UNDO'}

    # @classmethod
    # def poll(cls, context):
    #     return context.object.type == 'ARMATURE'
    
    def execute(self, context):
        #obj = context.object
        empties = []
        #Select only the bones with the constraints
        for obj in context.view_layer.objects:
            if obj.type != 'ARMATURE':
                continue
            for posebone, bone in zip(obj.pose.bones, obj.data.bones):
                bone.select = False
                for con in posebone.constraints:
                    if '_to_Ctrl' in con.name:
                        bone.select = True
                        obj.select_set(True)
                        empty = con.target
                        empties.append(empty)
                        empty.select_set(False)
                        if empty.parent is None:
                            break
                        if 'Empty_' in empty.parent.name and empty.parent.type == 'EMPTY' and empty.parent not in empties:
                            empties.append(empty.parent)
                        break

        #check frame range through all empty actions and scene frames
        start = int(min([empty.animation_data.action.frame_range[0] for empty in empties if empty.animation_data is not None]))
        end = int(max([empty.animation_data.action.frame_range[1] for empty in empties if empty.animation_data is not None]))

        bpy.ops.nla.bake(frame_start = start, frame_end = end, only_selected = True, visual_keying = True, clear_constraints=False, use_current_action=True, bake_types = {'POSE'})
        
        remove_constraints(context, obj)
        switch_to_original_obj(context, obj.animtoolbox.controlled)
        
        remove_ctrls()
        remove_collection()

        return {'FINISHED'}

def remove_collection():
    for col in bpy.data.collections:
        if 'Ctrl_Helpers' in col.name and not len(col.objects):
            bpy.data.collections.remove(col)

def remove_ctrls():
    
    for ctrl in bpy.data.objects:
        if 'bate' not in ctrl.keys(): #empty.type != 'EMPTY' or 
            continue
        if ctrl.animation_data is not None:
            if ctrl.animation_data.action is not None:
                bpy.data.actions.remove(ctrl.animation_data.action)
        bpy.data.objects.remove(ctrl)

def remove_constraints(context, obj):
    
    constrained_obj = None
    for obj in context.view_layer.objects:
        if obj.type != 'ARMATURE':
            continue
        for bone in obj.pose.bones:
            for con in bone.constraints:
                if not '_to_Ctrl' in con.name:
                    continue
                
                con_path = con.path_from_id()
                bone.constraints.remove(con)
                #select the original bone
                obj.data.bones[bone.name].select = True
                constrained_obj = obj
                #remove any fcurve channels of the constraints
                if obj.animation_data.action is None:
                    continue
                
                #remove fcurves related to the constraint
                fcurves = obj.animation_data.action.fcurves
                
                con_fcu = fcurves.find(con_path)
                if con_fcu is not None:
                    fcurves.remove(con_fcu)
                    
                #remove drivers connected to the constraint
                for drv in obj.animation_data.drivers:
                    if con_path in drv.data_path:
                        obj.animation_data.drivers.remove(drv)

    # if not constrained_obj:
    #     return None
    return constrained_obj


def switch_to_original_obj(context, original_obj):
    
    if original_obj is None:
        return
    original_obj.select_set(True)
    #print('switch to original ', original_obj.name, original_obj.select_get())
    context.view_layer.objects.active = original_obj
    if original_obj.type == 'ARMATURE':
        bpy.ops.object.mode_set(mode = 'POSE')
        
class Cleanup(bpy.types.Operator):
    """Select all the bones that are constrained to the empty"""
    bl_idname = "anim.remove_bones_constraints"
    bl_label = "Remove_Constrains_from_Bones"
    bl_options = {'REGISTER', 'UNDO'}

    # @classmethod
    # def poll(cls, context):
    #     bones = context.selected_pose_bones
    #     return len(bones) if bones is not None else False

    def execute(self, context):
        obj = context.object

                                
        if context.scene.btc.cleanup != 'EMPTIES':
            original_obj = remove_constraints(context, obj)    
            
        if context.scene.btc.cleanup == 'CONSTRAINTS':
            return {'FINISHED'}
        
        switch_to_original_obj(context, original_obj)

        #remove control rigs and empties
        remove_ctrls()

        remove_collection()  
        
        
        #print('finished ', context.object.name) 
        return {'FINISHED'}

def ctrl_shape_items(self, context):
    '''Add all the display items for bones and empties'''
    shapes = ['Cube','Sphere','Circle','Arrows','Plain Axes','Single Arrow','Cone']
    if context.scene.btc.controller == 'BONE':
        shapes.insert(0, 'Original')
        shapes.append('Bone')
    #create an item similiar to ('CIRCLE', 'Circle', 'Circle display type', 1)
    items = [(shape.upper(), shape, shape + ' display type', i) for i, shape in enumerate(shapes)]

    return items

def parent_object_update(self, context):
    '''checks if the previous selected bone is included in the current selected object'''
    if not self.parent_object:
        self.parent_bone = ''
        return
    if hasattr(self.parent_object, 'type'):
        if self.parent_object.type == 'ARMATURE':
            if self.parent_bone in self.parent_object.data.bones:
                return
    self.parent_bone = ''
    
    
def add_parent_ctrl(self, context):
    #Create the parent empty
    if not context.scene.btc.parent:
        return None
    parent = context.scene.btc.parent_object
    
    name = "Parent_Ctrl"
    if parent is not None: 
        if parent.mode == 'POSE' and context.scene.btc.parent_bone != '':
            #if context.scene.btc.parent_bone is not None:
            parent = parent.pose.bones[context.scene.btc.parent_bone]
        name += '_' + parent.name

    #if parent is not None:
    parent_empty = add_empty(self, parent, name)
    # parent_empty.name = "Empty_Parent"
    parent_empty.empty_display_size = self.display_size*2
    #constraint_add(parent_empty, parent, 'COPY_TRANSFORMS')
    parent_empty.select_set(False)
    
    return parent_empty

class ParentToCursor(bpy.types.Operator):
    """Create an empty parent at cursor"""
    bl_idname = "anim.parent_to_cursor"
    bl_label = "Parent_To_Cursor"
    bl_options = {'REGISTER', 'UNDO'}
    
    display_size: bpy.props.FloatProperty(name = 'Ctrl size', description="Display size of the ctrls", min = 0.0, max = 1.0, default = 0.5)
    display_type: bpy.props.EnumProperty(name = 'Display type', description="Display type for the controls", items = ctrl_shape_items)
    
    def execute(self, context):
        obj = context.object
        
        if obj.mode != 'POSE':
            return {'CANCELLED'}
        #parent_empty = add_empty(self, context.scene.cursor, 'Empty_Parent')
        rig = add_ctrl_rig(context.selected_pose_bones)
        #rig['Controller'] = bpy.props.StringProperty()
        if 'Child_of_Switch' not in rig.keys():
            rig['Child_of_Switch'] = 1.0
            rig.id_properties_ensure()  # Make sure the manager is updated
            property_manager = rig.id_properties_ui('Child_of_Switch')
            property_manager.update(min=0, max=1.0)
            #rig.Child_of_Switch = bpy.props.FloatProperty(name="Child_of_Switch", description="Layer Influence", min = 0.0, max = 1.0, default = 1.0, options={'ANIMATABLE'})
        
        # #Create the connection between the two objects
        # obj.animtoolbox.controller = rig
        # rig.animtoolbox.controlled = obj
        
        parentbone = add_rig_bone(rig, context.scene.cursor)
        for posebone in context.selected_pose_bones:
            con = constraint_add(posebone, parentbone, con_type = 'CHILD_OF')
            datapath = con.path_from_id() + '.influence'
            
            #adding the driver
            drv = obj.animation_data.drivers.find(datapath)
            
            if drv is None:
                drv = obj.animation_data.drivers.new(datapath)
            var = drv.driver.variables.new()
            var.name = con.name
            var.targets[0].id = rig
            var.targets[0].data_path = '["Child_of_Switch"]'
            frame_current = context.scene.frame_current
            
            #print(datapath)
            #add keyframe
            # obj.keyframe_insert(datapath, frame = frame_current)
            # obj.keyframe_insert(datapath, frame = frame_current-1)
            # fcu = obj.animation_data.action.fcurves.find(datapath)
            # fcu.keyframe_points[0].co[1] = 0
            # fcu.keyframe_points[1].co[1] = 1
            
            #add keyframe to the empty itself
            parentbone.keyframe_insert('location', frame = frame_current)
            parentbone.keyframe_insert('rotation_euler', frame = frame_current)
            
        return {'FINISHED'}


    
class ParentSwitch(bpy.types.Operator):
    """Switch the parent control on and off"""
    bl_idname = "anim.parentctrl_switch"
    bl_label = "Parent_Control_switch"
    bl_options = {'REGISTER', 'UNDO'}
    
    def execute(self, context):
        # obj = context.object
        
        # if obj.mode != 'POSE':
        #     return {'CANCELLED'}
        frame_current = context.scene.frame_current
        '''
        for empty in context.scene.objects:
            if empty.type == 'EMPTY' and 'Empty_Parent' in empty.name and 'bate' in empty:
                parent_empty = empty
                print(parent_empty.name)
                #add keyframe to the empty itself
                parent_empty.keyframe_insert('location', frame = frame_current)
                break
        '''
        for obj in context.scene.objects:
            if obj.type != 'ARMATURE':
                continue
            if 'Controller_rig' in obj.name and 'bate' in obj:
                #add keyframe to the empty itself
                for bone in obj.pose.bones:
                    datapath = bone.path_from_id()
                    rotation = '.rotation_quaternion' if bone.rotation_mode == 'QUATERNION' else '.rotation_euler'
                    obj.keyframe_insert(datapath + '.location', frame = frame_current)
                    obj.keyframe_insert(datapath + rotation, frame = frame_current)
                    break
            
            if not obj.select_get():
                continue        
            #add keys to the influence of the child of constraint
            for posebone in obj.pose.bones:
                for con in posebone.constraints:
                    if not '_to_Ctrl' in con.name and con.type != 'CHILD_OF':
                        continue
                    datapath = con.path_from_id() + '.influence'
                    obj.keyframe_insert(datapath, frame = frame_current)
                    obj.keyframe_insert(datapath, frame = frame_current-1)
                    fcu = obj.animation_data.action.fcurves.find(datapath)
                    
                    for keyframe in fcu.keyframe_points:
                        if keyframe.co[0] == frame_current:
                            keyframe.co[1] = float(not round(con.influence))
                        fcu.keyframe_points[0].co[1] = 0
                        fcu.keyframe_points[1].co[1] = 1
                    
        #viewport update    
        for area in context.screen.areas:
            if area.type in ['GRAPH_EDITOR','DOPESHEET_EDITOR']:
                area.tag_redraw()
            #fcu.update()
        return {'FINISHED'}

class BakeToControl(bpy.types.Operator):
    """Copy and bake the animation to temporary controls"""
    bl_idname = "anim.bake_to_ctrl"
    bl_label = "Bake_To_Controls"
    bl_options = {'REGISTER', 'UNDO'}

    frame_start: bpy.props.IntProperty(name = 'Start', description="The start frame for the bake")
    frame_end: bpy.props.IntProperty(name = 'End', description="The end frame for the bake")
    bake_loc: bpy.props.EnumProperty(name = 'Bake Location', description="Select which point on the bone to bake to", items = [('HEAD', 'Heads','Bake to the bones heads', 0), ('TAIL', 'Tails', 'Bake to the bones tails', 1)])
    con_back: bpy.props.BoolProperty(name = 'Constraint bones to Ctrls', description = "Constraint the selected bones back to the controls", default = True)
    child: bpy.props.BoolProperty(name = 'Add extra child Ctrls', description = "Add an child control for an overlay control", default = False)
    display_size: bpy.props.FloatProperty(name = 'Ctrl size', description="Display size of the controls", min = 0.0, default = 1.5)
    display_type: bpy.props.EnumProperty(name = 'Display type', description="Display type for the controls", items = ctrl_shape_items)

    @classmethod
    def poll(cls, context):
        return len(context.selected_objects)

    def invoke(self, context, event):
        obj = context.object
        posebones = context.selected_pose_bones if obj.mode == 'POSE' else []
        bonespath = [bone.path_from_id() for bone in posebones]
        wm = context.window_manager
        if obj.animation_data is None:
            return wm.invoke_props_dialog(self, width = 200)
        
        if obj.animation_data.action is None:
            self.frame_start, self.frame_end = context.scene.frame_start, context.scene.frame_end
        else:
            #get the frame range from the action and scene range
            self.frame_start = int(max(context.scene.frame_start, obj.animation_data.action.frame_range[0]))
            self.frame_end = int(min(context.scene.frame_end, obj.animation_data.action.frame_range[1]))
            #get the frame range from 
            if bonespath:
                frame_end = []
                frame_start = []
                for fcu in obj.animation_data.action.fcurves:
                    if any(path in fcu.data_path for path in bonespath):
                        frame_start.append(round(fcu.range()[0]))
                        frame_end.append(round(fcu.range()[1]))
                if frame_start:
                    self.frame_start = int(min(frame_start))
                    self.frame_end = int(max(frame_end))
            
            
        return wm.invoke_props_dialog(self, width = 200)

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.prop(self, 'display_size', text = 'Size')
        row.prop(self, 'display_type', text = '')

        layout.separator()
        row = layout.row()
        row.label(text = 'Frame Range: ')
        row = layout.row()
        row.prop(self, 'frame_start')
        row.prop(self, 'frame_end')
        
        if context.object.mode == 'POSE':
            split = layout.split(factor = 0.6)
            split.label(text = 'Bake Location: ')
            #row.prop_menu_enum(self, 'bake_loc')
            split.prop(self, 'bake_loc', text = '')
        layout.separator()
        row = layout.row()
        row.prop(self, 'con_back')
        row = layout.row()
        row.prop(self, 'child')

        # box = layout.box()
        # row = box.row(align = True)
        row = layout.row()
        row.prop(context.scene.btc, 'parent', text = 'Add parent Ctrl')
        if context.scene.btc.parent:
            # row = box.row(align = True)
            row = layout.row()
            row.prop(context.scene.btc, 'parent_object', text = '')
            if hasattr(context.scene.btc.parent_object, 'type'):
                if context.scene.btc.parent_object.type == 'ARMATURE':
                    # row = box.row(align = True)
                    row = layout.row()
                    row.prop_search(context.scene.btc, 'parent_bone', context.scene.btc.parent_object.pose, 'bones', text = '')

    def execute(self, context):
        obj = context.object
        # if obj.type != 'ARMATURE':
        #     return {'CANCELLED'}
        ctrls = []
        constraints = []
        controltype = context.scene.btc.controller
        if obj.mode == 'POSE':
            selected = context.selected_pose_bones
        else:
            selected = context.selected_objects
        #obj.select_set(False)
            
        con_type = 'COPY_TRANSFORMS'

        if controltype == 'EMPTY':
            ctrls = add_control_empties(self, selected) 
                    
            for ctrl, bone in zip(ctrls, selected):
                constraints = constraint_ctrl_to_bone(ctrl, bone, self.bake_loc, con_type, constraints)
                ctrl.select_set(True)
            bake_type = 'OBJECT'
            #obj.select_set(False)   
                  
        else: #add bone controls and constrain them
            rig = add_ctrl_rig(selected)
            ctrls, parent_bone = add_control_bones(self,selected, rig)
            if parent_bone:
                parent_name = parent_bone.name
            bpy.ops.object.mode_set(mode = 'POSE')
            ctrl_group = add_ctrl_group(rig)
            #convert to ctrl name to posebones
            ctrls = [rig.pose.bones[name] for name in ctrls]
            
            #constrain the parent bone
            if parent_bone:
                parent_bone = rig.pose.bones[parent_name]
                if parent_name in  obj.pose.bones:
                    parent_contsraint = constraint_add(parent_bone, obj.pose.bones[parent_name])
            
            for ctrl, bone in zip(ctrls, selected):
                constraints = constraint_ctrl_to_bone(ctrl, bone, self.bake_loc, con_type, constraints)
                #assign the controller to a group
                ctrl.bone_group = ctrl_group
                if self.child:
                    child_ctrl = ctrl.children[0]
                    child_ctrl.bone_group = ctrl_group
                if self.display_type == 'ORIGINAL':
                    #Add a shape and assign it to the control or child control via ctrlshape
                    original_shape_properties(ctrl, bone, self.display_size)
                    #add the original shape also to the child controls
                    if self.child:
                        original_shape_properties(child_ctrl, bone, self.display_size)
                    if ctrl.custom_shape_transform:
                        #constraint custom_shape_transform to original rig bone
                        ot_bone = ctrl.custom_shape_transform.name
                        #Get the original override transform bone
                        target = bone.id_data.pose.bones[ot_bone]
                        constraint_add(ctrl.custom_shape_transform, target, con_type)
                else:
                    wgt = draw_wgt(ctrl.length*self.display_size, bone, shape = self.display_type)
                    if wgt:
                        ctrl.custom_shape = wgt
                         #add the custom shape also to the child controls
                        if self.child:
                            child_ctrl.custom_shape = wgt
                        #keeps the original shape size
                        ctrl.use_custom_shape_bone_size = False
            
                bake_type = 'POSE'
                
            bpy.ops.object.mode_set(mode = bake_type)
            #make sure the rig is selected
            rig.select_set(True)
                
        obj.select_set(False)
        bpy.ops.nla.bake(frame_start = self.frame_start, frame_end = self.frame_end, only_selected = True, visual_keying = True, clear_constraints=True, use_current_action=True, bake_types = {bake_type})
        
        if parent_bone:
            rig.data.bones[parent_bone.name].select = False
            
        #Constrains the selected bones back to the controls
        if not self.con_back:
            return {'FINISHED'}
        #select which constraints are going to be added
        con_type = 'DAMPED_TRACK' if self.bake_loc == 'TAIL' else 'COPY_TRANSFORMS'
        #for empty, emptychild, bone in zip(empties, child_empties, selected_bones):
        for ctrl, bone in zip(ctrls, selected):
            if self.child: #switch to the child controling the original bone
                ctrl = ctrl.children[0]
            constraint_add(bone, ctrl, con_type)

        return {'FINISHED'}


classes = (BakeToControl, ParentToCursor, ParentSwitch, SelectConstrainedBones, Cleanup, BakeConstraints)

def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)  
    
def unregister():
    from bpy.utils import unregister_class
    for cls in classes:
        unregister_class(cls)
